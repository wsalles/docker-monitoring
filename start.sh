#!/bin/bash
#Criacao das pastas
servicos=(prometheus alertmanager)
subpastas=(data config)
stack="monitoring"

criarPasta() {
        echo "#1) CRIANDO PASTA"
	for x in "${servicos[@]}"
	do
	  if mkdir "./"$x; then
	    chmod -R 755 "./"$x
	    sleep 1;
	    for y in "${subpastas[@]}"
	    do
	      if mkdir "./"$x"/"$y; then
	        chmod -R 755 "./"$x"/"$y
	        sleep 1;        
	      else
	        echo "A pasta "$x"/"$y" existe!"
	      fi
	   done
	  else
	    echo "A pasta "$x" existe!"
	  fi
	done
}

installDocker() {
        echo "#2) INSTALL DOCKER"
	if mkdir /etc/docker; then
          echo "Instalando o Docker...!"
	  curl -fsSL https://get.docker.com | sh
	  docker swarm init
	else
          echo "O Docker ja esta instalado!"
	  sleep 1;
        fi
}

installNetdata() {
        echo "#3) INSTALL NETDATA"
	if mkdir /etc/netdata; then
          echo "Instalando o Netdata...!"
	  bash <(curl -Ss https://my-netdata.io/kickstart.sh)
	else
          echo "O Netdata ja esta instalado!"
	  sleep 1;
        fi
}

dockerImages() {
        prefix="spla"
	images=(prometheus alertmanager node-exporter)
	echo "#4) CRIANDO AS IMAGENS DOS CONTAINERS"
	for x in "${images[@]}"
	do
	  docker images | grep $prefix/$x
	  if [ $? -eq 0 ]; then
	    echo "A imagem: "$prefix"/"$x" ja existe!"
	  else
	    docker build -t $prefix/$x:latest ./dockerfiles/$x/.
	    echo "A imagem: "$prefix"/"$x" foi criada!"
	  fi
	done
}

destruction() {
        docker stack ls | grep $stack
        if [ $? -eq 0 ]; then
          docker stack rm $stack
          echo "Aguardando 5s..."
          sleep 5;
        else
          sleep 1;
          echo "#####################################"
          echo "## OS CONTAINERS NAO ESTAO RODANDO ##"
          echo "##           nada a fazer          ## "
          echo "#####################################"
        fi
}


deploy() {
	docker stack deploy -c docker-compose.yml monitoring

}

criarPasta;
installDocker;
installNetdata;
dockerImages;
destruction;
deploy;
